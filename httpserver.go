package httpserver

import (
	"io"
	"net"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"
)

type Hdr struct {
	K string
	V []byte
}

func ParseRequest(buf []byte, n int, hdrs []Hdr) (method, path, query, ver, _1stChunk []byte) {
	hdrsLen := len(hdrs)
	if buf == nil || n < 256 || hdrsLen > 32 {
		return
	}
	bgn, end := 0, 0
	for end < n && buf[end] != ' ' {
		end++
	}
	method = buf[0:end]
	end++
	bgn = end
	for end < n && buf[end] != ' ' {
		if buf[end] == '?' {
			path = buf[bgn:end]
			end++
			bgn = end
		} else {
			end++
		}
	}
	if path != nil {
		query = buf[bgn:end]
	} else if end < n {
		path = buf[bgn:end]
	}
	end++
	bgn = end
	for end < n && buf[end] != '\r' {
		end++
	}
	if end < n {
		ver = buf[bgn:end]
	}
	end += 2
	bgn = end
	idx := [...]int{
		0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17,
		18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
	}
	for end < n && buf[end] != '\r' {
		for end < n && buf[end] != ':' {
			if buf[end] > ('A'-1) && buf[end] < ('Z'+1) {
				buf[end] |= 0b00100000 // to lower case
			}
			end++
		}
		end += 2
		pos := end
		for end < n && buf[end] != '\r' {
			end++
		}
		if pos < end && end < n {
			for x := 0; x < hdrsLen; x++ {
				i := idx[x]
				if string(buf[bgn:pos-2]) == hdrs[i].K {
					hdrs[i].V = buf[pos:end]
					copy(idx[x:], idx[x+1:])
					hdrsLen--
				}
			}
		}
		end += 2
		bgn = end
	}
	if (end + 2) < n {
		_1stChunk = buf[end+2 : n]
	}
	return
}

type (
	Route[T any] map[string]map[string]func(net.Conn, []byte, int, T)
)

func (r Route[T]) LookUp(method, path []byte) (hndlr func(net.Conn, []byte, int, T)) {
	if pthHndlr, ok := r[string(method)]; ok {
		if hndlr, ok = pthHndlr[string(path)]; ok {
			return
		}
	}
	hndlr = NotFound[T]
	return
}
func GetFile[T any](flNm, ct string) func(net.Conn, []byte, int, T) {
	pool := sync.Pool{
		New: func() interface{} {
			file, err := os.Open(flNm)
			if err != nil {
				println(err.Error())
				return nil
			}
			return file
		},
	}
	info, err := os.Stat(flNm)
	if err != nil {
		println(err.Error())
		return nil
	}
	_1stLine := "HTTP/1.1 200 OK\r\n"
	date := _1stLine + "Content-Type: " + ct + "\r\nDate: "
	datePos := len(date)
	hdrs := date + http.TimeFormat + "\r\nContent-Length: "
	bfr := make([]byte, 1024)
	n := copy(bfr, hdrs)
	n += copy(bfr[n:], strconv.Itoa(int(info.Size())))
	n += copy(bfr[n:], "\r\n\r\n")
	return func(conn net.Conn, _ []byte, _ int, _ T) {
		copy(bfr[datePos:], time.Now().Format(http.TimeFormat))
		conn.Write(bfr[:n])
		file_ := pool.Get()
		defer pool.Put(file_)
		file, ok := file_.(*os.File)
		if !ok {
			return
		}
		file.Seek(0, 0)
		io.Copy(conn, file)
	}
}

var notFound = []byte("HTTP/1.1 404 Not Found\r\nContent-Type: text/plain\r\nContent-Length: 9\r\n\r\nnot found")

func NotFound[T any](conn net.Conn, _ []byte, _ int, _ T) {
	conn.Write(notFound)
}
